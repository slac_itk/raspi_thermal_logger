from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, Sequence, UnicodeText, DateTime
from sqlalchemy.orm import sessionmaker
import json
Base = declarative_base()

class  Image(Base):
    __tablename__ = 'thermal'
    id = Column(Integer, Sequence('image_id_seq'), primary_key=True)
    barcode = Column(String(50))
    data = Column(String(length=2**16))
    created=Column('time', DateTime, default=datetime.datetime.utcnow)
    def __init__(self, barcode, data):
        self.barcode = barcode
        self.data = data
Base.metadata.create_all(engine)

class database:
    def __init__(self,url):        
        self.session=sessionmaker()
        Session.configure(bind=engine)
        self.engine = create_engine(url)
    def write(self,self,frame):
        session = self.session()
        frame_j=json.dumps(self,frame)
        new_image=Image(self.code,frame_j)
        session.add(new_image)
        session.commit()
    def read():
        pass



