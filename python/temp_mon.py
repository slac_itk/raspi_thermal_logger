#!/usr/bin/python3
from PyQt5 import QtWidgets
from PyQt5 import QtCore
from PyQt5 import QtGui
from pyqtgraph import PlotWidget, plot
import PIL
import pyqtgraph as pg
import sys
import os
import math
import numpy as np
import board
import busio
import adafruit_mlx90640
import datetime
import json
import cv2
import tempfile
import elog
import yaml


INTERPOLATE = 1
heatmap = (
    (0.0, (0, 0, 0)),
    (0.20, (0, 0, 0.5)),
    (0.40, (0, 0.5, 0)),
    (0.60, (0.5, 0, 0)),
    (0.80, (0.75, 0.75, 0)),
    (0.90, (1.0, 0.75, 0)),
    (1.00, (1.0, 1.0, 1.0)),
)

# how many color values we can have
COLORDEPTH = 128

colormap = [0] * COLORDEPTH

# some utility functions
def constrain(val, min_val, max_val):
    return min(max_val, max(min_val, val))
def gaussian(x, a, b, c, d=0):
    return a * math.exp(-((x - b) ** 2) / (2 * c ** 2)) + d


def gradient(x, width, cmap, spread=1):
    width = float(width)
    r = sum(
        [gaussian(x, p[1][0], p[0] * width, width / (spread * len(cmap))) for p in cmap]
    )
    g = sum(
        [gaussian(x, p[1][1], p[0] * width, width / (spread * len(cmap))) for p in cmap]
    )
    b = sum(
        [gaussian(x, p[1][2], p[0] * width, width / (spread * len(cmap))) for p in cmap]
    )
    r = int(constrain(r * 255, 0, 255))
    g = int(constrain(g * 255, 0, 255))
    b = int(constrain(b * 255, 0, 255))
    return r, g, b

colorpos=COLORDEPTH*[0]
for i in range(COLORDEPTH):
    colormap[i] = (*gradient(i, COLORDEPTH, heatmap),255)
    colorpos[i]=1./(COLORDEPTH-1)*i
    
class MainWindow(QtWidgets.QMainWindow):
    def __init__(self,app,*args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        top = QtWidgets.QVBoxLayout()
        self.app=app
        self.dT=0
        self.mintemp=0
        self.maxtemp=0
        self.T=0
        w= QtWidgets.QWidget()        
        w.setLayout(top)
        self.setCentralWidget(w)
        self.statusLine=QtWidgets.QLabel("dT= ")
        pos=   np.array(colorpos)
        color = np.array(colormap, dtype=np.ubyte)
        maps = pg.ColorMap(pos, color)
        lut = maps.getLookupTable(0.0, 1.0, 256)
        top.addWidget(self.statusLine)
        win = pg.GraphicsLayoutWidget()
        win.resize(320,200)
        win.setBackground('w')
        #self.setCentralWidget(win)
        view = pg.ViewBox(border=None)
        win.addItem(view)
        view.setAspectLocked(True)
        self.img = pg.ImageItem(border=None)
        self.img.setLookupTable(lut)
        view.addItem(self.img)
        gl = pg.GradientLegend((15, 300), (2.5, -30))
        gl.setGradient(maps.getGradient())
        gl.setParentItem(view)
        top.addWidget(win)
#        self.setCentralWidget(win)
#        self.setLayout(top)
        self.gl=gl
        i2c = busio.I2C(board.SCL, board.SDA)
        self.mlx = adafruit_mlx90640.MLX90640(i2c)
        self.mlx.refresh_rate = adafruit_mlx90640.RefreshRate.REFRESH_0_5_HZ
        self.frame=frame = [0] * 768
        QtCore.QTimer.singleShot(2000, self.read)
        self.code=""
    def read(self):
        for retry in range(10):
            try:
                self.mlx.getFrame(self.frame)
                break
            except ValueError:
                continue 
        
        sensorout=np.empty((32,24),dtype=float)
        for h in range(24):
            for w in range(32):
                pixel = self.frame[h * 32 + w]
                sensorout[w, h]= pixel
        
         
        ## Threshold the -40C to 300 C temps to a more human range
        # Sensor seems to read a bit cold, calibrate in final setting
        rangeMin = 15 #min(f)-5 # low threshold temp in C
        rangeMax = 35 #max(f) # high threshold temp in C


        # Apply thresholds based on min and max ranges
        depth_scale_factor = 255.0 / (rangeMax-rangeMin)
        depth_scale_beta_factor = -rangeMin*255.0/(rangeMax-rangeMin)
        imgIR=sensorout
        depth_uint8 = imgIR*depth_scale_factor+depth_scale_beta_factor
        depth_uint8[depth_uint8>255] = 255
        depth_uint8[depth_uint8<0] = 0
        depth_uint8 = depth_uint8.astype('uint8')
        displayIR = cv2.resize(imgIR, dsize=(32*8,24*16), interpolation=cv2.INTER_CUBIC)
        bigIR = cv2.resize(depth_uint8, dsize=(32*10,24*10), interpolation=cv2.INTER_CUBIC)

        # Normalize the image
        normIR = cv2.normalize(bigIR, bigIR, 0, 255, cv2.NORM_MINMAX, cv2.CV_8U)

        # Apply a color heat map
        colorIR = cv2.applyColorMap(normIR, cv2.COLORMAP_JET)

        # Use a bilateral filter to blur while hopefully retaining edges
        brightBlurIR = cv2.bilateralFilter(normIR,9,150,150)

        # Threshold the image to black and whitsys.path.insert(0, "./build/lib.linux-armv7l-3.5")e 
        retval, threshIR = cv2.threshold(brightBlurIR, 210, 255, cv2.THRESH_BINARY)

        # Define kernal for erosion and dilation and closing operations
        kernel = np.ones((5,5),np.uint8)

        erosionIR = cv2.erode(threshIR,kernel,iterations = 1)

        dilationIR = cv2.dilate(erosionIR,kernel,iterations = 1)

        closingIR = cv2.morphologyEx(dilationIR, cv2.MORPH_CLOSE, kernel)

        # Detect edges with Canny detection, currently only for visual testing not counting
        edgesIR = cv2.Canny(closingIR,50,70, L2gradient=True)

        # Detect countours
        #contours, hierarchy = cv2.findContours(closingIR, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)[-2:]

        # Get the number of contours ( contours count when touching edge of image while blobs don't)
        ROI_number = 0
        cnts = cv2.findContours(closingIR, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
        cnts = cnts[0] if len(cnts) == 2 else cnts[1]
        
        f=self.frame
        area=0
        t0=None
        for c in cnts:
            # Obtain bounding rectangle to get measurements
            x,y,w,h = cv2.boundingRect(c)
            if(x==0 and y==0): continue
            a=w*h
            # Find centroid
            a=w*h
            x1=x+w/2
            y1=y+h/2
            x1=int(x1/10)
            y1=int(y1/10)
            temp = f[(23-y1) * 32 + x1]
            
            if(a>area):
                area=a
                t0=temp
            #print(x1,23-y1,w,h,temp)

        steps=5
        MAXTEMP=int(np.amax(sensorout))
        MINTEMP=int(np.amin(sensorout))
        maxtemp=np.amax(sensorout)
        mintemp=np.amin(sensorout)
        delta=(MAXTEMP-MINTEMP)/steps
        if delta<1:
            steps=MAXTEMP-MINTEMP
            delta=(MAXTEMP-MINTEMP)/steps
        
        labels=dict()
        s=0
        i=MINTEMP
        for s in range(steps+1):
            l="%02d" %i
            labels[l]=1/(steps)*s
            i=i+delta
        self.gl.setLabels(labels)
        self.img.setImage(displayIR,pos=(50,0))
        dt=maxtemp-mintemp
        T=(maxtemp+mintemp)/2
        self.statusLine.setText("T= %02.2f/%02.2f/%02.2f, dT= %02.2f"%(mintemp,T,maxtemp,dt))
        self.dT=dt
        self.mintemp=mintemp
        self.maxtemp=maxtemp
        self.T=T
        QtCore.QTimer.singleShot(2000, self.read)

    def log_data(self,key):
        j=dict()
        j["data"]=self.frame
        j["T"]=self.T
        j["dT"]=self.dT
        j["T_min"]=self.mintemp
        j["T_max"]=self.maxtemp
        json_file=tempfile.mktemp(".json")
        png_file=tempfile.mktemp(".png")
        geo=self.frameGeometry()
        self.app.primaryScreen().grabWindow(0,geo.x(),geo.y(),geo.width(),geo.height()).save(png_file)
        f=open(json_file,"w")
        f.write(json.dumps(j))
        f.close()
        cf=open("config/config.yaml")
        config=yaml.load(cf)
        cf.close()
        f1=open(json_file,"rb")
        f2=open(png_file,"rb")        
        msg={
            'Barcode': key,
            'author':'scanner',
            'Category':'Temperature',
            'Subject': "T=%02.2f,  dT=%02.2f,  Tmin=%02.2f,  Tmax=%02.2f"%(self.T,self.dT,self.mintemp,self.maxtemp)
            }
        subject="T=%02.2f,  dT=%02.2f,  Tmin=%02.2f,  Tmax=%02.2f"%(self.T,self.dT,self.mintemp,self.maxtemp)
        print(config['elog_host'])
        logbook = elog.open(hostname="https://"+config['elog_host'],logbook='Glue Syringe Tracking',
                            port=config['elog_port'],
                            user=config['elog_user'],
                            password=config['elog_password'],use_ssl=True)
        id=logbook.search({'Barcode' : key })
        if(len(id)==0 or id is None):            
            logbook.post(subject, reply=False,attributes=msg,attachments=[f1,f2])
        else:
            logbook.post(subject, msg_id=id[0], reply=True,attributes=msg,attachments=[f1,f2])
        f1.close()
        f2.close()
        os.remove(json_file)
        os.remove(png_file)
        
    def keyPressEvent(self, event):
         if type(event) == QtGui.QKeyEvent:
             #here accept the event and do something             
             key=event.key()
             event.accept()
             if(key<256):
                 self.code+=chr(key)
             elif(key&0xff==4):
                 try:
                     print("log")
                     #session = Session()
                     #frame_j=json.dumps(self.frame)
                     #new_image=Image(self.code,frame_j)
                     #session.add(new_image)
                     #session.commit()
                     print(self.code)
                     self.log_data(self.code)
                 except Exception as err:
                     print("DB error:",err)
                 self.code=""
         else:
             event.ignore()
        

             
def main():
    #QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, False)
    #QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_Use96Dpi, True)
    os.chdir(os.path.dirname(os.path.dirname(sys.argv[0])))
    app = QtWidgets.QApplication(sys.argv)
    font_db=QtGui.QFontDatabase()
    fontfile="./fonts/Oxygen-Bold.ttf"
    font_id=font_db.addApplicationFont(fontfile)
    font=font_db.font("Oxygen","Bold",32)
    app.setFont(font)
    main = MainWindow(app)
    main.setFont(font)
    main.showFullScreen()
#    main.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
