MLX_modules=True
try:
    import board
    import busio
    import adafruit_mlx90640
except:
    MLX_modules=False
class IR_array:
    def __init__(self,sim=False):
        
        i2c = busio.I2C(board.SCL, board.SDA)
        self.mlx = adafruit_mlx90640.MLX90640(i2c)
        self.mlx.refresh_rate = adafruit_mlx90640.RefreshRate.REFRESH_0_5_HZ
        self.frame=frame = [0] * 768
    
def read(self):
    frame=None
    for retry in range(10):
        try:
            self.mlx.getFrame(self.frame)
            break
        except ValueError:
            continue
    return self.frame
